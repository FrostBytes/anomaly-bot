# Anomaly
Anomaly is an open-source Discord bot backend. Currently, the only hosted
versions in existance are SCP-079 (see the scp079 branch) and The Man. The
SCP-079 bot is currently for public use. 

[Invite the bot to your server](https://discord.com/api/oauth2/authorize?client_id=498885756874391554&permissions=0&scope=bot).

By default, the bot does not have any permissions. To use the bot's features,
you must grant them manually.k
