-- Table is used to keep track of user's guild-specific data, such as EXP.
CREATE TABLE local_users (
    guild_id BIGINT UNSIGNED NOT NULL,
    user_id BIGINT UNSIGNED NOT NULL,
    xp INT NOT NULL DEFAULT 0,
    last_message BIGINT UNSIGNED NOT NULL,
    PRIMARY KEY (guild_id, user_id)
);

-- Table is used to keep track of guild specific items, such as autoroles and
-- levelled roles.
CREATE TABLE guilds (
    guild_id BIGINT UNSIGNED NOT NULL,
    -- See the anomaly storage module for more information on how these values
    -- are stored. This can store a max of 4 autoroles
    auto_roles VARBINARY(32) NOT NULL,
    -- This can store a max of 32 levelled roles
    level_roles VARBINARY(384) NOT NULL,
    PRIMARY KEY (guild_id)
);
