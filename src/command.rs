use std::error::Error;
use std::fmt::{Debug, Display, Formatter, Error as FmtError};

/// An error that can occur during parsing of a user command.
pub enum CommandError {
    /// A command was not provided (prefix only).
    MissingCommand,
    /// The message is not a command.
    NoCommand,
}

impl Display for CommandError {
    fn fmt(&self, f: &mut Formatter) -> Result<(), FmtError> {
        match self {
            CommandError::MissingCommand =>
                write!(f, "no command provided!"),
            CommandError::NoCommand =>
                write!(f, "message is not a command"),
        }
    }
}

impl Debug for CommandError {
    fn fmt(&self, f: &mut Formatter) -> Result<(), FmtError> {
        <Self as Display>::fmt(self, f)
    }
}

impl Error for CommandError {}

/// An error that can occur during parsing of an argument.
pub struct ArgumentError<T>
where T: std::error::Error + 'static {
    pos: i32,
    error: T,
}

impl<T> ArgumentError<T>
where T: std::error::Error + 'static {
    /// Creates a new error.
    pub fn new(error: T, pos: i32) -> ArgumentError<T> {
        ArgumentError {
            error,
            pos,
        }
    }

    /// Get a reference to the internal error.
    pub fn inner(&self) -> &T {
        &self.error
    }

    /// Get the position of the argument that errored.
    pub fn pos(&self) -> i32 {
        self.pos
    }
}

impl<T> Display for ArgumentError<T>
where T: std::error::Error + 'static {
    fn fmt(&self, f: &mut Formatter) -> Result<(), FmtError> {
        write!(f, "error at argument {}, {}", self.pos, self.error)
    }
}

impl<T> Debug for ArgumentError<T>
where T: std::error::Error + 'static {
    fn fmt(&self, f: &mut Formatter) -> Result<(), FmtError> {
        <Self as Display>::fmt(self, f)
    }
}

impl<T> std::error::Error for ArgumentError<T>
where T: std::error::Error + 'static {
    fn source(&self) -> Option<&(dyn std::error::Error + 'static)> {
        Some(&self.error)
    }
}

/// Trait that converts from a command context to a type.
pub trait FromArg<'arg>
where Self: Sized {
    type Error: std::error::Error + 'static;

    /// Preforms the conversion.
    fn from_arg(arg: &'arg str) -> Result<Self, Self::Error>;
}

impl<'a> FromArg<'a> for &'a str {
    type Error = !;

    fn from_arg(arg: &'a str) -> Result<Self, !> {
        Ok(arg)
    }
}

impl<'a> FromArg<'a> for String {
    type Error = !;

    fn from_arg(arg: &'a str) -> Result<Self, !> {
        Ok(arg.to_string())
    }
}

/// An iterator over command arguments.
pub struct ArgsIterator<'a> {
    source: &'a str,
    pos: i32,
    cursor: usize,
}

impl<'a> ArgsIterator<'a> {
    /// Create a new args iterator.
    pub fn new(source: &'a str) -> ArgsIterator<'a> {
        let mut iter = ArgsIterator {
            source,
            pos: 0,
            cursor: 0,
        };

        iter.skip_whitespace();
        iter
    }

    /// Get the next argument.
    pub fn next<T>(&mut self) -> Option<Result<T, ArgumentError<T::Error>>>
    where T: FromArg<'a> {
        Some(T::from_arg(self.next_raw()?).map_err(|e| ArgumentError::new(e, self.pos)))
    }

    /// Gets the position of the last argument read.
    pub fn pos(&self) -> i32 {
        self.pos
    }

    /// Gets the rest of the command and converts it into `T`.
    pub fn tail<T>(&self) -> Option<Result<T, ArgumentError<T::Error>>> 
    where T: FromArg<'a> {
        if self.cursor >= self.source.len() {
            None
        } else {
            Some(T::from_arg(&self.source[self.cursor..]).map_err(|e| ArgumentError::new(e, self.pos)))
        }
    }

    fn next_raw(&mut self) -> Option<&'a str> {
        if self.cursor >= self.source.len() {
            None
        } else {
            self.pos += 1;
            let start = self.cursor;

            while self.cursor < self.source.len() {
                let ch = self.source[self.cursor..].chars().next().unwrap();

                if ch.is_whitespace() {
                    let span = &self.source[start..self.cursor];

                    self.skip_whitespace();

                    return Some(span);
                }
            }

            Some(&self.source[start..])
        }
    }

    fn skip_whitespace(&mut self) {
        while self.cursor < self.source.len() {
            let ch = self.source[self.cursor..].chars().next().unwrap();

            if ch.is_whitespace() {
                self.cursor += ch.len_utf8();
            } else {
                break;
            }
        }
    }
}

/// A helper struct for parsing commands.
pub struct Command<'a> {
    cmd: &'a str,
    args: ArgsIterator<'a>,
}

impl<'a> Command<'a> {
    /// Parse a command from a message.
    pub fn parse(
        prefix: &str, 
        content: &'a str
    ) -> Result<Command<'a>, CommandError> {
        // check if the command has a valid prefix
        if content.starts_with(prefix) {
            // clip out the prefix
            let full_cmd = &content[prefix.len()..];

            if full_cmd.len() > 0 {
                // read the command
                let mut iter = full_cmd.splitn(2, char::is_whitespace);

                // read cmd and args
                Ok(Command {
                    cmd: iter.next().unwrap(),
                    args: ArgsIterator::new(match iter.next() {
                        Some(args) => args,
                        None => "",
                    }),
                })
            } else {
                Err(CommandError::MissingCommand)
            }
        } else {
            Err(CommandError::NoCommand)
        }
    }

    /// The command (first word).
    ///
    /// Spaces cannot be escaped within quotation marks.
    pub fn cmd(&self) -> &'a str {
        self.cmd
    }

    /// Obtain a reference to the underlying [`ArgsIterator`].
    pub fn args(&mut self) -> &mut ArgsIterator<'a> {
        &mut self.args
    }
}
