use std::path::Path;
use std::error::Error;
use std::fmt::{Debug, Display, Formatter, Error as FmtError};
use std::fs::File;
use std::io::{Error as IoError, Read as _, Write as _};

use serde::{Deserialize, Serialize};

use toml::de::Error as TomlError;

/// An error that can occur doing config reading.
pub enum ConfigReadError {
    Io(IoError),
    Toml(TomlError),
    NotExist,
}

impl From<IoError> for ConfigReadError {
    fn from(e: IoError) -> ConfigReadError {
        ConfigReadError::Io(e)
    }
}

impl From<TomlError> for ConfigReadError {
    fn from(e: TomlError) -> ConfigReadError {
        ConfigReadError::Toml(e)
    }
}

impl Display for ConfigReadError {
    fn fmt(&self, f: &mut Formatter) -> Result<(), FmtError> {
        match self {
            ConfigReadError::Io(err) => write!(f, "{}", err),
            ConfigReadError::Toml(err) => write!(f, "{}", err),
            ConfigReadError::NotExist => write!(f, "config file does not exist!"),
        }
    }
}

impl Debug for ConfigReadError {
    fn fmt(&self, f: &mut Formatter) -> Result<(), FmtError> {
        <Self as Display>::fmt(self, f)
    }
}

impl Error for ConfigReadError {
    fn source(&self) -> Option<&(dyn Error + 'static)> {
        match self {
            ConfigReadError::Io(err) => Some(err),
            ConfigReadError::Toml(err) => Some(err),
            _ => None,
        }
    }
}

/// Config struct.
#[derive(Serialize, Deserialize)]
pub struct Configuration {
    #[serde(rename = "discord_token")]
    pub bot_token: String,
    #[serde(rename = "command_prefix")]
    pub bot_prefix: String,
    pub database_url: String,
}

impl Default for Configuration {
    fn default() -> Configuration {
        Configuration {
            bot_token: String::from("<DISCORD BOT TOKEN>"),
            bot_prefix: String::from("."),
            database_url: String::from("<DATABASE URL>"),
        }
    }
}

/// Find a config file in the absolute directory.
pub fn read_config<T>(path: T) -> Result<Configuration, ConfigReadError> 
where T: AsRef<Path> {
    let mut path = path.as_ref().to_path_buf();
    path.push("config.toml");

    let mut file = match File::open(path) {
        Ok(file) => file,
        Err(err) => match err.kind() {
            std::io::ErrorKind::NotFound => return Err(ConfigReadError::NotExist),
            _ => return Err(err.into()),
        }
    };
    let mut buf = String::with_capacity(512);

    file.read_to_string(&mut buf)?;

    Ok(toml::de::from_str(&buf)?)
}

/// Write a config file that does not exist.
pub fn make_config<T>(path: T) -> Result<(), IoError> 
where T: AsRef<Path> {
    let mut path = path.as_ref().to_path_buf();
    path.push("config.toml");

    let mut file = File::create(path)?;
    file.write(toml::ser::to_string(&Configuration::default()).unwrap().as_bytes())?;

    Ok(())
}
