use rand::RngCore as _;
use rand::rngs::OsRng;

use std::slice::from_raw_parts_mut;

/// Check how much experience is required to reach the passed level.
pub fn level_to_exp(level: f64) -> f64 {
    64.0 * level * level
}

/// Check what level a user would be at with `exp` experience.
pub fn exp_to_level(exp: f64) -> f64 {
    (exp / 64.0).sqrt()
}

/// Get a random amount of exp.
pub fn rng_exp() -> i32 {
    // this function uses unsafe code for a extremely small speed boost
    let mut num: u32 = 0;
    let ptr = &mut num as *mut u32 as *mut u8;

    OsRng.fill_bytes(unsafe {
        from_raw_parts_mut(ptr, std::mem::size_of::<u32>())
    });
    (num % 6 + 10) as i32
}
