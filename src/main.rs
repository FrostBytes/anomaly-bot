use anomaly::config::{Configuration, ConfigReadError, make_config, read_config};
use anomaly::command::Command;
use anomaly::storage::users;
use anomaly::levelling::{exp_to_level, level_to_exp, rng_exp};

use serenity::model::{channel::Message, gateway::Ready, id::{GuildId, UserId}};
use serenity::prelude::*;

use std::process::exit;
use std::time::{SystemTime, UNIX_EPOCH};
use std::iter;

use mysql::Pool;

pub const PROGRESS_BAR_LENGTH: usize = 20;

struct Handler {
    config: Configuration,
    sql: Pool,
}

impl Handler {
    pub fn new(config: Configuration) -> Handler {
        Handler {
            sql: Pool::new(&config.database_url).unwrap(),
            config,
        }
    }

    fn do_exp(&self, msg: &Message, guild_id: GuildId) {
        // obtain a connection to sql
        let mut conn = self.sql.get_conn().unwrap();
        let now = SystemTime::now().duration_since(UNIX_EPOCH).expect("Time went backwards").as_secs();
        let record = match users::info(&mut conn, *guild_id.as_u64(), *msg.author.id.as_u64()) {
            Ok(record) => record,
            Err(err) => { eprintln!("{} (failed to award exp)", err); return; },
        };
        
        // check if the user has finished their cooldown
        if record.cooldown < now {
            let xp = rng_exp();

            // the cooldown is finished! award the user exp
            match users::award(&mut conn, *guild_id.as_u64(), *msg.author.id.as_u64(), record.xp + xp, now + 20) {
                Ok(()) => (),
                Err(err) => eprintln!("{} (failed to award exp)", err),
            }
        }
    }

    fn level_check(&self, ctx: Context, msg: Message, guild_id: GuildId) {
        // obtain a connection to sql
        let mut conn = self.sql.get_conn().unwrap();

        // get the info of the local user
        let user = match users::info(&mut conn, *guild_id.as_u64(), *msg.author.id.as_u64()) {
            Ok(user) => user,
            Err(err) => {
                eprintln!("{} (failed to get user information from .level)", err);
                return;
            },
        };

        let _ = msg.channel_id.send_message(
            &ctx.http,
            |m| m.embed(|e| {
                let xp = user.xp as f64;
                let current_level = exp_to_level(xp).floor();
                let next_level = current_level + 1.0;

                let min_cur_level = level_to_exp(current_level);
                let min_next_level = level_to_exp(next_level);
                let progress = ((xp - min_cur_level) / (min_next_level - min_cur_level) * PROGRESS_BAR_LENGTH as f64).round() as usize;

                e.color(0x7289DA)
                    .title(format!("level information for user {}", msg.author.name))
                    .description(format!(
                        "```\nlv {} [{}{}] lv {}\n{}xp total\n{}xp until next level```", 
                        current_level as i32, 
                        iter::repeat('=').take(progress).collect::<String>(),
                        iter::repeat('-').take(PROGRESS_BAR_LENGTH - progress).collect::<String>(),
                        next_level as i32,
                        user.xp,
                        (min_next_level - xp) as i32,
                    ))
            }),
        );
    }

    fn top_check(&self, ctx: Context, msg: Message, guild_id: GuildId) {
        // obtain a connection to sql
        let mut conn = self.sql.get_conn().unwrap();

        // get the list of local users
        let iter = match users::list(&mut conn, *guild_id.as_u64(), 10) {
            Ok(res) => res,
            Err(err) => {
                eprintln!("{} (failed to get a list of users)", err);
                return;
            },
        };

        let _ = msg.channel_id.send_message(
            &ctx.http,
            |m| m.embed(|e| {
                e.color(0x7289DA)
                    .title("who is at the top?")
                    .description(iter.map(|user| {
                        match user {
                            Ok(user) => {
                                let lock = ctx.cache.read();
                                let duser = lock.users.get(&UserId(user.user_id));
                                
                                if let Some(duser) = duser {
                                    format!(
                                        "{:<10}xp at lv{:<3} : {}",
                                        user.xp,
                                        exp_to_level(user.xp as f64).floor() as i32,
                                        duser.read().name,
                                    )
                                } else {
                                    format!(
                                        "{:<10}xp at lv{:<3} : <removed user>",
                                        user.xp,
                                        exp_to_level(user.xp as f64).floor() as i32,
                                    )
                                }
                            },
                            Err(err) => {
                                eprintln!("{} (failed to get a certain user from a `.top` command)", err);
                                String::from("<error>")
                            }
                        }
                    }).fold(String::with_capacity(512), |acc, x| {
                        if acc.is_empty() {
                            acc + "```\n" + &x
                        } else {
                            acc + "\n\n" + &x
                        }
                    }) + "\n```")
            })
        );
    }
}

impl EventHandler for Handler {
    fn message(&self, ctx: Context, msg: Message) {
        // do not do anything to bot commands
        if msg.author.bot {
            return;
        }

        let guild_id = match msg.guild_id {
            Some(guild_id) => guild_id,
            None => return,
        };

        // do exp pass
        self.do_exp(&msg, guild_id);

        // convert the message into a command
        let command = match Command::parse(&self.config.bot_prefix, &msg.content) {
            Ok(command) => command,
            // ignore any other error
            _ => return,
        };

        match command.cmd() {
            // put possible commands here!
            "level" => self.level_check(ctx, msg, guild_id),
            "top" => self.top_check(ctx, msg, guild_id),
            "invite" => {
                let _ = msg.channel_id.say(&ctx.http, "https://discord.com/api/oauth2/authorize?client_id=498885756874391554&permissions=0&scope=bot");
            },
            // if no existing command was given, do not respond (it might be
            // another bot's command!)
            _ => (),
        }
    }

    fn ready(&self, _: Context, ready: Ready) {
        println!("connected as {} successfully", ready.user.name);
    }
}

fn main() {
    let cur_dir = std::env::current_dir().unwrap();
    let config = match read_config(&cur_dir) {
        Ok(config) => config,
        Err(err) => match err {
            ConfigReadError::NotExist => {
                make_config(&cur_dir).unwrap();

                println!("a default config was auto-generated for you");
                println!("to get this bot working, you must put your discord \
                          bot token in \"discord_token\" of config.toml");
                exit(0);
            },
            ConfigReadError::Io(err) => {
                eprintln!("{}", err);
                eprintln!("cannot read config! exiting...");
                exit(1);
            },
            ConfigReadError::Toml(err) => {
                eprintln!("{}", err);
                eprintln!("cannot read config! exiting...");
                exit(1);
            },
        }
    };

    let bot_token = String::from(&config.bot_token);
    let mut client = Client::new(bot_token, Handler::new(config)).expect("failed to create client instance");

    if let Err(err) = client.start() {
        eprintln!("Client error:\n{}", err);
    }
}
