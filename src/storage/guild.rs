use mysql::prelude::{ConvIr, FromValue, Queryable};
use mysql::{FromValueError, Value};

use super::DatabaseError;

/// A role list stores a list of roles in binary.
///
/// Little endian order only! Big endian is for chumps.
pub struct RoleList {
    inner: Vec<u8>,
}

impl RoleList {
    /// Add a role id to the role list.
    pub fn push(&mut self, id: u64) {
        self.inner.extend(id.to_le_bytes().iter());
    }

    /// Remove a role id from the role list.
    pub fn remove(&mut self, id: u64) {
        if let Some(i) = self.iter().position(|x| x == id) {
            let mut new = Vec::<u8>::with_capacity(self.inner.len() - 8);
            new.extend(&self.inner[..(i*8)]);
            new.extend(&self.inner[(i*8+8)..]);
            std::mem::swap(&mut self.inner, &mut new);
        }
    }
    
    /// Gets an iterator over all the roles in this role list.
    pub fn iter<'a>(&'a self) -> RoleListIter<'a> {
        RoleListIter {
            slice: &self.inner,
            cursor: 0,
        }
    }
}

impl ConvIr<RoleList> for RoleList {
    fn new(v: Value) -> Result<RoleList, FromValueError> {
        Ok(RoleList {
            inner: match v {
                Value::Bytes(v) => v,
                _ => return Err(FromValueError(v)),
            },
        })
    }

    fn commit(self) -> RoleList {
        self
    }

    fn rollback(self) -> Value {
        Value::Bytes(self.inner)
    }
}

impl FromValue for RoleList {
    type Intermediate = RoleList;
}

/// An iterator over a [`RoleList`].
pub struct RoleListIter<'a> {
    slice: &'a [u8],
    cursor: usize,
}

impl<'a> Iterator for RoleListIter<'a> {
    type Item = u64;

    fn next(&mut self) -> Option<u64> {
        if self.cursor >= self.slice.len() {
            return None;
        }

        let mut buf = [0u8; 8];
        buf.clone_from_slice(&self.slice[self.cursor..]);

        self.cursor += 8;

        Some(u64::from_le_bytes(buf))
    }
}

/// A level role list stores a list of roles, intended to be given at certain
/// experience levels, in binary.
///
/// To get all of the roles awarded to a certain level, one can use 
/// `list.iter().filter(|(_, x)| x == level)`.
pub struct LevelRoleList {
    inner: Vec<u8>
}

impl LevelRoleList {
    /// Add a role:level pair to the list.
    pub fn push(&mut self, id: u64, level: i32) {
        self.inner.reserve(12);
        self.inner.extend(id.to_le_bytes().iter());
        self.inner.extend(level.to_le_bytes().iter());
    }

    /// Removes a role:level pair by role id from the list.
    pub fn remove(&mut self, id: u64) {
        if let Some(i) = self.iter().position(|(x, _)| x == id) {
            let mut new = Vec::<u8>::with_capacity(self.inner.len() - 12);
            new.extend(&self.inner[..(i*12)]);
            new.extend(&self.inner[(i*12+12)..]);
            std::mem::swap(&mut self.inner, &mut new);
        }
    }

    /// Gets an iterator over the role:level pairs in the list.
    pub fn iter<'a>(&'a self) -> LevelRoleListIter<'a> {
        LevelRoleListIter {
            slice: &self.inner,
            cursor: 0,
        }
    }
}

impl ConvIr<LevelRoleList> for LevelRoleList {
    fn new(v: Value) -> Result<LevelRoleList, FromValueError> {
        Ok(LevelRoleList {
            inner: match v {
                Value::Bytes(v) => v,
                _ => return Err(FromValueError(v)),
            },
        })
    }

    fn commit(self) -> LevelRoleList {
        self
    }

    fn rollback(self) -> Value {
        Value::Bytes(self.inner)
    }
}

impl FromValue for LevelRoleList {
    type Intermediate = LevelRoleList;
}

/// An iterator over a [`LevelRoleList`]
pub struct LevelRoleListIter<'a> {
    slice: &'a [u8],
    cursor: usize,
}

impl<'a> Iterator for LevelRoleListIter<'a> {
    type Item = (u64, i32);

    fn next(&mut self) -> Option<(u64, i32)> {
        if self.cursor >= self.slice.len() {
            return None;
        }

        let mut buf_id = [0u8; 8];
        let mut buf_lv = [0u8; 4];
        buf_id.clone_from_slice(&self.slice[self.cursor..]);
        buf_lv.clone_from_slice(&self.slice[self.cursor+8..]);

        self.cursor += 12;

        Some((u64::from_le_bytes(buf_id), i32::from_le_bytes(buf_lv)))
    }
}

/// Create a new guild record.
pub fn new_guild<T>(conn: &mut T, id: u64) -> Result<(), DatabaseError> 
where T: Queryable {
    conn.exec_drop(
        "INSERT INTO guilds VALUES (?, '', '')",
        (id,),
    ).map_err(|err| DatabaseError::Internal(err))
}

/// Gets the guild's list of auto-roles.
pub fn guild_auto_roles<T>(conn: &mut T, id: u64) -> Result<RoleList, DatabaseError>
where T: Queryable {
    match conn.exec_first::<RoleList, _, _>(
        "SELECT auto_roles FROM guilds WHERE guild_id = ?",
        (id,),
    ).map_err(|err| DatabaseError::Internal(err))? {
        Some(list) => Ok(list),
        None => Err(DatabaseError::NotFound),
    }
}

/// Gets the guild's list of level-roles.
pub fn guild_level_roles<T>(conn: &mut T, id: u64) -> Result<LevelRoleList, DatabaseError>
where T: Queryable {
    match conn.exec_first::<LevelRoleList, _, _>(
        "SELECT level_roles FROM guilds WHERE guild_id = ?",
        (id,),
    ).map_err(|err| DatabaseError::Internal(err))? {
        Some(list) => Ok(list),
        None => Err(DatabaseError::NotFound),
    }
}
