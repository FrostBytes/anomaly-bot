pub mod guild;
pub mod users;

use std::error::Error;
use std::fmt::{Display, Debug, Formatter, Error as FmtError};

use mysql::Error as SqlError;

/// An error that can occur during database accesses.
pub enum DatabaseError {
    /// The model was not found/does not exist in the database.
    NotFound,
    /// An internal error occured.
    Internal(SqlError),
}

impl From<SqlError> for DatabaseError {
    fn from(e: SqlError) -> DatabaseError {
        DatabaseError::Internal(e)
    }
}

impl Display for DatabaseError {
    fn fmt(&self, f: &mut Formatter) -> Result<(), FmtError> {
        match self {
            DatabaseError::NotFound =>
                write!(f, "model was not found in the database"),
            DatabaseError::Internal(err) =>
                write!(f, "internal error: {}", err),
        }
    }
}

impl Debug for DatabaseError {
    fn fmt(&self, f: &mut Formatter) -> Result<(), FmtError> {
        <Self as Display>::fmt(self, f)
    }
}

impl Error for DatabaseError {
    fn source(&self) -> Option<&(dyn Error + 'static)> {
        match self {
            DatabaseError::Internal(err) => Some(err),
            _ => None,
        }
    }
}
