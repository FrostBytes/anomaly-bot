use mysql::prelude::{FromRow, Queryable};
use mysql::{Row, FromRowError};

use super::DatabaseError;

/// A list of local users, implemented as an iterator.
pub struct LocalUsersIterator<'c, 't, 'tc> {
    inner: mysql::QueryResult<'c, 't, 'tc, mysql::Binary>,
}

impl<'c, 't, 'tc> From<mysql::QueryResult<'c, 't, 'tc, mysql::Binary>> for LocalUsersIterator<'c, 't, 'tc> {
    fn from(q: mysql::QueryResult<'c, 't, 'tc, mysql::Binary>) -> LocalUsersIterator<'c, 't, 'tc> {
        LocalUsersIterator {
            inner: q,
        }
    }
}

impl<'c, 't, 'tc> Iterator for LocalUsersIterator<'c, 't, 'tc> {
    type Item = Result<LocalUser, DatabaseError>;

    fn next(&mut self) -> Option<Self::Item> {
        let mut row = match self.inner.next()? {
            Ok(row) => row,
            Err(err) => return Some(Err(err.into())),
        };
        Some(Ok(LocalUser {
            guild_id: row.take::<u64, _>("guild_id")?,
            user_id: row.take::<u64, _>("user_id")?, 
            xp: row.take::<i32, _>("xp")?,
            cooldown: row.take::<u64, _>("last_message")?,
        }))
    }
}

/// A struct representing a user record.
pub struct LocalUser {
    pub guild_id: u64,
    pub user_id: u64,
    pub xp: i32,
    pub cooldown: u64,
}

impl FromRow for LocalUser {
    fn from_row_opt(mut row: Row) -> Result<Self, FromRowError> {
        Ok(LocalUser {
            guild_id: match row.take::<u64, _>("guild_id") {
                Some(guild_id) => guild_id,
                None => return Err(FromRowError(row)),
            },
            user_id: match row.take::<u64, _>("user_id") {
                Some(user_id) => user_id,
                None => return Err(FromRowError(row)),
            },
            xp: match row.take::<i32, _>("xp") {
                Some(xp) => xp,
                None => return Err(FromRowError(row)),
            },
            cooldown: match row.take::<u64, _>("last_message") {
                Some(cooldown) => cooldown,
                None => return Err(FromRowError(row)),
            },
        })
    }
}

/// Create a new local user record.
pub fn new<T>(conn: &mut T, guild_id: u64, user_id: u64, init_xp: i32, cooldown: u64) -> Result<(), DatabaseError> 
where T: Queryable {
    conn.exec_drop(
        "INSERT INTO local_users VALUES (?, ?, ?, ?)",
        (guild_id, user_id, init_xp, cooldown),
    ).map_err(|err| err.into())
}

/// Get the amount of xp a user possesses and the cooldown.
///
/// If the user does not exist, this will return a 0.
pub fn info<T>(conn: &mut T, guild_id: u64, user_id: u64) -> Result<LocalUser, DatabaseError>
where T: Queryable {
    match conn.exec_first::<LocalUser, _, _>(
        "SELECT * FROM local_users WHERE guild_id = ? AND user_id = ?",
        (guild_id, user_id),
    ).map_err(|err| -> DatabaseError { err.into() })? {
        Some(xp) => Ok(xp),
        None => Ok(LocalUser {
            guild_id,
            user_id,
            xp: 0,
            cooldown: 0,
        }),
    }
}

/// Set the user's XP and cooldown value.
///
/// This function does not care if a record does not already exist; it will
/// make one if it doesn't exist.
pub fn award<T>(conn: &mut T, guild_id: u64, user_id: u64, xp: i32, cooldown: u64) -> Result<(), DatabaseError>
where T: Queryable {
    let changed = conn.exec_iter(
        "UPDATE local_users SET xp = ?, last_message = ? WHERE guild_id = ? AND user_id = ?",
        (xp, cooldown, guild_id, user_id),
    )?.affected_rows();

    if changed == 0 {
        // create a new record
        new(conn, guild_id, user_id, xp, cooldown)
    } else {
        Ok(())
    }
}

/// Gets a decending list of users by xp count.
pub fn list<T>(conn: &mut T, guild_id: u64, max: usize) -> Result<LocalUsersIterator, DatabaseError> 
where T: Queryable {
    conn.exec_iter(
        "SELECT * FROM local_users WHERE guild_id = ? ORDER BY xp DESC LIMIT ?",
        (guild_id, max),
    ).map_err(|err| err.into()).map(|i| i.into())
}

